@extends('layouts.full-width')

@section('banner')

	@php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
	@endphp

	<div class="banner-no-home">

		@if (!empty($banner_img_check))
			<img src="{{ $banner_img_check }}">
		@else
			<img src="{{ asset2('images/banner-trang-trong.jpg') }}">
		@endif

	</div>

@endsection

@section('content')

		@include('partials.page-header')
				
		<div class="category">
			<div class="container">
				<div class="row">

		        	<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 category-news" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000">
		        		<div class="category-content">
							<div class="cat-title">
								<h3><?php _e('News news','khanhminh'); ?></h3>
							</div>

				        	@php
				                $shortcode = "[listing per_page=4 orderby => 'date' layout='partials.content-tin-tuc']";
				                echo do_shortcode($shortcode);
				            @endphp
				        </div>
				    </div>

				    {{ view('sidebar') }}

		        </div>
		    </div>
		</div>

@endsection
