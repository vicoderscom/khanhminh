@extends('layouts.full-width')

@php
	$current_category = get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id = $current_category->term_id;
	$cat_name = get_cat_name($cat_id);
@endphp

@section('banner')

	@php
        $image_cat = get_field('image_cat','category_'.$cat_id.'');
		$banner_img_check = $image_cat['url'];
	@endphp

	<div class="banner-no-home">

		@if (!empty($banner_img_check))
			<img src="{{ $banner_img_check }}">
		@else
			<img src="{{ asset2('images/banner-trang-trong.jpg') }}">
		@endif

	</div>

@endsection

@section('content')

	<div class="page-header">
		<h1>{{ $cat_name }}</h1>
	</div>

	<div class="category">
		<div class="container">

        	<div class="category-news">
        		<div class="category-content">
  
					@php
				        $shortcode = "[listing cat=$cat_id layout='partials.content-tin-tuc' paged='yes' per_page='2']";
						echo do_shortcode($shortcode);
					@endphp

		        </div>
		    </div>

	    </div>
	</div>

@endsection
