@extends('layouts.full-width')

@section('banner')

	@php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
	@endphp

	<div class="banner-no-home">

		@if (!empty($banner_img_check))
			<img src="{{ $banner_img_check }}">
		@else
			<img src="{{ asset2('images/banner-trang-trong.jpg') }}">
		@endif

	</div>

@endsection

@section('content')
    @while(have_posts())

		{!! the_post() !!}

        @include('partials.page-header')
        
        <div class="contact">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 contact-form-contact">
	        			{!! wpautop(the_content()) !!}
				        
				        @php 
	                        dynamic_sidebar('contact-form');
	                    @endphp
				    </div>
				    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 contact-map">
				    	@php 
	                        dynamic_sidebar('contact-map');
	                    @endphp
				    </div>
				</div>
			</div>
		</div>
        
    @endwhile
@endsection
