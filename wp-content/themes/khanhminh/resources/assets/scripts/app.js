import 'jquery';
import 'bootstrap';
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';
import aos from 'aos/dist/aos.js';
import 'slick-carousel';

import 'fancybox';

$(document).ready(function() {

aos.init();

    $('body').click(function(event){
        $(this).find('.search-box form input').fadeOut();
    });
	$('.search-box form .search-icon').click(function(event){
		$(this).parent().find('input').fadeToggle();
        event.stopPropagation();//ko tính click body
	});
    $('.search-box form input').click(function(event){
        $(this).fadeIn(); 
        event.stopPropagation();
    });

//$(window).resize(function() {
    var width_body = $('body').width();
    if(width_body <= 1024) {

    	$('.language').click(function(){
    		$(this).find('#lang_sel_list').slideToggle();
    	});
    }
//});
	
    var elements = $("input, select");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                 e.target.setCustomValidity(e.target.getAttribute("requiredmsg"));
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
    

    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });


    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 20) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
            $('html, body').animate({
            scrollTop: $('html, body').offset().top
            }, 1000);
        });
    }

    $('.banner-home .msc-listing').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
    });

    $('.duan-content .item .row').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]        
    });

    $('.chung-nhan .row').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });

});




