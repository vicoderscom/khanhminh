<article class="item">

<figure>
	<img src="{{ getPostImage(get_the_ID(), 'expert') }}" alt="{{ $title }}" />
</figure>

<div class="info">
	<div class="cat-title">
	    <h3>{{ $title }}</h3>
	</div>
	<div class="desc">
		{!! wpautop($content) !!}
	</div>
</div>

</article>


