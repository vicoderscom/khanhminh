<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class NewsLook extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('newslook', 'khanhminh'),
            'label'       => __('See more news', 'khanhminh'),
            'description' => __('See more news', 'khanhminh'),
        ];

        $fields = [
            [
                'label' => __('Number post', 'khanhminh'),
                'name'  => 'number_post',
                'type'  => 'number',
            ]        
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="cat-title">
                <h3>
                    <?php _e('See more news','khanhminh'); ?>
                </h3>
            </div>
        <?php

        $data = [
            'number_post' => $instance['number_post']
        ];

        view('partials.NewsLook', $data);
                            
	}
}
