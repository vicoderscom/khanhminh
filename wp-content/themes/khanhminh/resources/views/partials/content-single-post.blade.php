<div>

    <div class="single">
        <div class="container">
            <div class="single-content">
                <div>
                    <h1 class="entry-title">{{ get_the_title() }}</h1>
                </div>

                <div class="entry-content">
                    {!! wpautop(the_content()) !!}
                </div>
            </div>
        </div>
    </div>
                    
    @php setPostViews(get_the_ID()) @endphp
                    
</div>
