<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class BannerHome extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'bannerhome';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'BannerHome';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'BannerHome';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-art'];

}
