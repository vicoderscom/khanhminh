<?php

namespace App\Providers;

use App\Widgets\SampleWidget;
use App\Widgets\NewsNews;
use App\Widgets\NewsLook;

use Illuminate\Support\ServiceProvider;

class WidgetServiceProvider extends ServiceProvider
{
    public $listen = [
        SampleWidget::class,
        NewsNews::class,
        NewsLook::class,
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveWidget($class);
        }
    }

    /**
     * Resolve a widget instance from the class name.
     *
     * @param  string  $widget
     * @return widget instance
     */
    public function resolveWidget($widget)
    {
        return new $widget();
    }
}
